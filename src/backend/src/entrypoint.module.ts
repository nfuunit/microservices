import { Module } from '@nestjs/common';
import { NewsController } from './modules/news/news.controller';
import { NewsService } from './modules/news/news.service';

@Module({
  imports: [],
  controllers: [NewsController],
  providers: [NewsService],
})
export class EntrypointModule {}
