import { Controller, Get, Res, HttpStatus, Param } from '@nestjs/common';
import { NewsService } from './news.service';

@Controller('/news')
export class NewsController {
  constructor(private readonly storage: NewsService) {}

  @Get('/')
  async getAll(@Res() response) {
    const items = this.storage.getAll();
    return response.status(HttpStatus.OK).json(items);
  }
  @Get('/get_by_id/:id')
  async getById(@Res() response, @Param('id') id) {
    const item = this.storage.getById(+id);
    if (!item) {
      return response.status(HttpStatus.NOT_FOUND).send();
    }
    return response.status(HttpStatus.OK).json(item);
  }
  @Get('/get_by_alias/:alias')
  async getByAlais(@Res() response, @Param('alias') alias) {
    const item = this.storage.getByAlias(alias);
    if (!item) {
      return response.status(HttpStatus.NOT_FOUND).send();
    }
    return response.status(HttpStatus.OK).json(item);
  }
}
