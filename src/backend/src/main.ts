import { NestFactory } from '@nestjs/core';
import { EntrypointModule } from './entrypoint.module';

async function bootstrap() {
  const app = await NestFactory.create(EntrypointModule, { cors: true });
  await app.listen(3000);
}
bootstrap();
