import React, { useEffect, useState } from 'react';
import Api from '../../services/Api';
import NewsItem from '../../dto/NewsItem';
import NewsItemComponent from '../../components/news-item/NewsItem';
import LoaderComponent from '../../components/loader/Loader';
import './Home.css';

const Home: React.FC = () => {
    const [items, setItems] = useState<NewsItem[]>([]);
    const [is_loaded, setIsLoaded] = useState<boolean>(false);
    useEffect(() => {
        console.log('111');
        async function fetchNews() {
            try {
                const data = await Api.getNewsList();
                setItems(data);
            } catch (error) {
                console.error(error);
            } finally {
                setIsLoaded(true);
            }
        }

        fetchNews()
    }, [])
  return (
    <>
        <h1>Home</h1>
        {!is_loaded && <LoaderComponent />}
        {is_loaded && items.map((item, index) => (
            <NewsItemComponent key={index} item={item} />
        ))}
    </>
  )
}

export default Home
