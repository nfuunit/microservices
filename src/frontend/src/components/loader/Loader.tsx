import React from "react";
import './Loader.css';
import spinner from '../../assets/loading-spinner.svg';

const LoaderComponent: React.FC = () => {
    return (
        <div id="loader" className="loader">
            <img src={spinner} alt="Loading" />
        </div>
    );
}

export default LoaderComponent;
